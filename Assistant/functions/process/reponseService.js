'use strict';
const {
    Image,
    BrowseCarousel,
    BrowseCarouselItem
} = require('actions-on-google');
const getVelibAvailability = require('../service/bikeProcess');
const getStationAvailability = require('../service/stationProcess');
const getGeoCoordinate = require('../service/geoCoordinatesService');

const GOOLE_MAPS_LOGO_URL = "https://lh3.googleusercontent.com/MOf9Kxxkj7GvyZlTZOnUzuYv0JAweEhlxJX6gslQvbvlhLK5_bSTK6duxY2xfbBsj43H=s180";
const GOOGLE_MAPS_URL = 'https://www.google.fr/maps/search/';

function generateReponse(conversation, coordinates, CITY) {

    let stationBrowseItem, bikeBrowseItem;
    conversation.ask("Cliquer sur un item pour lancer le GPS. Le premier item vous guidera à la station avec des vélos disponibles, le second à des emplacements pour poser votre vélo");

    return new Promise((resolve, reject) => {
        const promise =
            getVelibAvailability(coordinates, CITY)
                .catch(failureCallback)
                .then(res => {
                    bikeBrowseItem = new BrowseCarouselItem({
                        title: res.name,
                        description: 'Google Assistant on Android and iOS',
                        image: new Image({
                            url: GOOLE_MAPS_LOGO_URL,
                            alt: 'Google Maps',
                        }),
                        url: GOOGLE_MAPS_URL + res.position.lat + ',+' + res.position.lng,
                        footer: 'More information about the Google Assistant',
                    });

                    return getStationAvailability(coordinates, CITY)
                })
                .catch(failureCallback)
                .then(result => {
                    stationBrowseItem = new BrowseCarouselItem({
                        title: result.name,
                        description: 'Google Assistant on Android and iOS',
                        image: new Image({
                            url: GOOLE_MAPS_LOGO_URL,
                            alt: 'Google Maps',
                        }),
                        url: GOOGLE_MAPS_URL + result.position.lat + ',+' + result.position.lng,
                        footer: 'More information about the Google Assistant',
                    });

                    conversation.add(new BrowseCarousel({
                        items: [bikeBrowseItem, stationBrowseItem],
                    }));

                    conversation.close();
                    return conversation
                });
        resolve(promise);

    })
}


function getConv(conversation, coordinates, CITY, adresse) {
    if (coordinates != null) {
        return generateReponse(conversation, coordinates, CITY);
    } else {
        coordinates = new Promise((resolve, reject) => {
            getGeoCoordinate(adresse, CITY)
                .then(res => {
                    resolve(res)
                });
        });
        return generateReponse(conversation, coordinates, CITY);

    }
}


function failureCallback(error) {
    console.log("RreponseService : L'opération a échoué avec le message : " + error);
    return error;
}

module.exports = getConv;

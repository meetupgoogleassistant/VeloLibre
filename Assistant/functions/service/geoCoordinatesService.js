'use strict';
const myModule = require('../model/geoPoint');

const GOOGLE_MAPS_API = 'AIzaSyBxYDI-lLUuidr04Yc-TaFn3LwtwtA2wzw';

const request = require('request');

const WRONG_ADDRESS_ERROR_CODE = 1;

const utf8 = require('utf8');

async function getGeoCoordinate(address, city) {
    address.concat(' ', city);
    return await new Promise((resolve, reject) => {
        let regex = / /gm;
        address = address.replace(regex, '+');
        address =  utf8.encode(address);
        const url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + ',+CA&key=' + GOOGLE_MAPS_API;
        console.log(url);
        request(url, {json: true}, (err, res, body) => {
            if (err) return reject(err);
            if (body.results[0] !== undefined) {
                let lat = body.results[0].geometry.location.lat;
                let lng = body.results[0].geometry.location.lng;
                let location = new myModule.GeoPoint(lat, lng);
                return resolve(location);
            }
            else return reject(WRONG_ADDRESS_ERROR_CODE)
        })
    })

}


module.exports = getGeoCoordinate;


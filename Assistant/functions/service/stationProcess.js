'use strict';
const request = require('request');
const API_URL = require('../config/index').api;
const NO_VELIBS_FINDS_ERROR_CODE = 2;


async function getStationAvailability(location, city) {
    return await new Promise((resolve, reject) => {

        let url = API_URL + 'station/' + city + '/point=';

        url += JSON.stringify(location);
        console.log(url);
        request(url, {json: true}, (err, res, body) => {
            if (err) return reject(err);
            if (body !== undefined) return resolve(body);
            else return reject(NO_VELIBS_FINDS_ERROR_CODE)

        })

    })

}

function failureCallback(error) {
    console.log("L'opération a échoué avec le message : " + error);
    return error;
}


module.exports = getStationAvailability;
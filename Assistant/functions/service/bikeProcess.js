'use strict';

const request = require('request');
const API_URL = require('../config/index').api;

const NO_VELIBS_FINDS_ERROR_CODE = 2;

async function getVelibAvailability(location, city) {
    return await new Promise((resolve, reject) => {
        let url = API_URL+'velo/'+city+'/point=';
        url += JSON.stringify(location);
        console.log(url);
        request(url, {json: true}, (err, res, body) => {
            if (err) {
                console.log(err);
                return reject(err);
            }
            if (body !== undefined) {
                console.log(body);
                return resolve(body);
            }
            else {
                console.error(res);
                return reject(NO_VELIBS_FINDS_ERROR_CODE)
            }
        })
    })
}



module.exports = getVelibAvailability;

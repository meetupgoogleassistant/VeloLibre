'use strict';

const functions = require('firebase-functions');
const {
    dialogflow,
    Permission,
    Suggestions
} = require('actions-on-google');
const getConv = require('./process/reponseService');
const app = dialogflow({
    debug: true
});


exports.bikeSearcherFulfillment = functions.https.onRequest(app);


function welcome(conversation) {

    console.log("search velib");

    conversation.data.requestedPermission = 'DEVICE_PRECISE_LOCATION';
    conversation.ask("Bonjour ! ");
    conversation.ask("Je peux vous aider à trouver un vélo en libre-service");
    conversation.ask(new Permission({
        context: 'Pouvez-vous me donner accès à votre position pour vous proposer la station la plus proche de vous ',
        permissions: conversation.data.requestedPermission,
    }));
    conversation.ask(new Suggestions(['Oui', 'Non']));
    return conversation
}

function fallback(conversation) {
    conversation.ask(`J'ai mal compris votre demande.`);
    return conversation

}


async function permissionHandler(conversation, params, confirmationGranted) {
    console.log("search velib");

    const CITY = JSON.stringify(conversation.headers.city).split('"').join('');


    if (confirmationGranted) {
        const {
            coordinates
        } = conversation.device.location;
        conversation = await new Promise((resolve, reject) => {
            getConv(conversation, coordinates, CITY)
                .then(res => {
                    resolve(res);
                }).catch(failureCallback);

        });
    } else {
        conversation.ask(`C'est dommage sans votre position il sera plus compliqué pour moi de trouver les meilleures stations. Pouvez-vous me donner l'adresse ou vous souhaitez prendre votre vélo ?`);
        conversation.contexts.set('no-gps-data', 2);
    }
    return conversation;
}

async function addresseHandler(conversation, parameters) {
    let locations = Object.values(parameters.location);
    const CITY = JSON.stringify(conversation.headers.city).split('"').join('');
    let adresse = '';
    locations.forEach(function (adresselement) {
        if (adresselement !== '') adresse = adresse.concat(' ', adresselement);
    });
    conversation = await new Promise((resolve, reject) => {
        getConv(conversation, null, CITY, adresse)
            .then(res => {
                resolve(res);
            }).catch(failureCallback);

    });
    return conversation
}


function failureCallback(error) {
    console.log("L'opération a échoué avec le message : " + error);
    return error;
}

app.intent('Default Welcome Intent', welcome);
app.intent('Default Fallback Intent', fallback);
app.intent('Permission Intent', permissionHandler);
app.intent('searchVelib', permissionHandler);
app.intent('Adresse Intent', addresseHandler);


function GeoPoint(lat, lng) {
    // allow instantiation without 'new'
    if (!(this instanceof GeoPoint)) return new GeoPoint(lat, lng);

    this.lat = Number(lat);
    this.lng = Number(lng);
}



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if (typeof module !== 'undefined' && module.exports) module.exports.GeoPoint = GeoPoint; // ≡ export default LatLon
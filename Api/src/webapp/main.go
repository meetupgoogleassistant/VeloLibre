package main

import (
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"net/http"
	"p2l-developper/velib-api/src/controlers"
)

func main() {

	router := mux.NewRouter()
	router.HandleFunc("/velo/{city}/polygon={polygon}", controlers.GetVelibsForAPolygon).Methods("GET")
	router.HandleFunc("/velo/{city}/point={point}", controlers.GetVelibsForAPoint).Methods("GET")
	router.HandleFunc("/station/{city}/point={point}", controlers.GetStationForAPoint).Methods("GET")

	http.Handle("/", router)

	appengine.Main()

}

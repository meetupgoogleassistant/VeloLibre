package controlers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
	"io/ioutil"
	"net/http"
	"p2l-developper/velib-api/src/geometry"
	"p2l-developper/velib-api/src/model"
)

var API_KEY = "155f52de763531f36328e4dd27ce0829ccb2da1f"

type listGeoposition struct {
	List []model.GeoAddress `json:"list,omitempty"`
}

func GetVelibsForAPolygon(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintln(w, "GetVelibsForAPolygon \n")

	params := []byte(mux.Vars(r)["polygon"])
	city := mux.Vars(r)["city"]
	fmt.Printf("%s \n", city)

	var polygon listGeoposition

	json.Unmarshal(params, &polygon)

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	response, err := client.Get("https://api.jcdecaux.com/vls/v1/stations?contract=nantes&apiKey=" + API_KEY)
	if err != nil {
		fmt.Printf("%s \n", err)
		//os.Exit(1)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s \n", err)
			//os.Exit(1)
		}
		fmt.Printf("2nd \n  %s \n", string(contents))

		var stations, finalstations []model.Station

		json.Unmarshal(contents, &stations)

		for _, station := range stations {

			tmp := geometry.Coordinate_is_inside_polygon(station.Position, polygon.List)

			if tmp {
				finalstations = append(finalstations, station)
			}

		}

		json.NewEncoder(w).Encode(finalstations)

	}

}

func GetVelibsForAPoint(w http.ResponseWriter, r *http.Request) {

	params := []byte(mux.Vars(r)["point"])
	city := mux.Vars(r)["city"]
	fmt.Printf("%s \n", city)

	var point model.GeoAddress

	json.Unmarshal(params, &point)

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	response, err := client.Get("https://api.jcdecaux.com/vls/v1/stations?contract=" + city + "&apiKey=" + API_KEY)
	if err != nil {
		fmt.Printf("%s \n", err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s \n", err)
			//os.Exit(1)
		}
		fmt.Printf("2nd \n  %s \n", string(contents))

		var stations []model.Station
		var finalstation model.Station
		json.Unmarshal(contents, &stations)
		finalstation = geometry.FindClosestBike(stations, point)

		json.NewEncoder(w).Encode(finalstation)

	}

}

package controlers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
	"io/ioutil"
	"net/http"
	"p2l-developper/velib-api/src/geometry"
	"p2l-developper/velib-api/src/model"
)

func GetStationForAPoint(w http.ResponseWriter, r *http.Request) {

	params := []byte(mux.Vars(r)["point"])
	city := mux.Vars(r)["city"]
	fmt.Printf("%s \n", city)

	var point model.GeoAddress

	json.Unmarshal(params, &point)

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	response, err := client.Get("https://api.jcdecaux.com/vls/v1/stations?contract=" + city + "&apiKey=" + API_KEY)
	if err != nil {
		fmt.Printf("%s \n", err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Printf("%s \n", err)
		}
		fmt.Printf("2nd \n  %s \n", string(contents))
		var stations []model.Station
		var finalstation model.Station
		json.Unmarshal(contents, &stations)
		finalstation = geometry.FindClosestStation(stations, point)
		json.NewEncoder(w).Encode(finalstation)
	}
}

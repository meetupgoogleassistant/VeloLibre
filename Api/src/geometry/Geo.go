package geometry

import (
	"math"
	"p2l-developper/velib-api/src/model"
	"sort"
)

const TWOPI = 2 * math.Pi

func Coordinate_is_inside_polygon(geoPoint model.GeoAddress, polygon []model.GeoAddress) bool {
	var angle float64
	angle = 0
	var point1_lat float64
	var point1_long float64
	var point2_lat float64
	var point2_long float64
	n := len(polygon)

	for i := 0; i < n; i++ {
		point1_lat = polygon[i].GetLatitude() - geoPoint.GetLatitude()
		point1_long = polygon[i].GetLongitude() - geoPoint.GetLongitude()
		point2_lat = polygon[(i+1)%n].GetLatitude() - geoPoint.GetLatitude()
		//you should have paid more attention in high school geometry.
		point2_long = polygon[(i+1)%n].GetLongitude() - geoPoint.GetLongitude()
		angle += Angle2D(point1_lat, point1_long, point2_lat, point2_long)
	}
	if math.Abs(angle) < math.Pi {
		return false
	} else {
		return true
	}

}

func Angle2D(y1 float64, x1 float64, y2 float64, x2 float64) float64 {
	var dtheta, theta1, theta2 float64

	theta1 = math.Atan2(y1, x1)
	theta2 = math.Atan2(y2, x2)
	dtheta = theta2 - theta1

	for dtheta > math.Pi {
		dtheta -= TWOPI
	}
	for dtheta < -math.Pi {
		dtheta += TWOPI
	}

	return dtheta

}

func is_valid_gps_coordinate(latitude float64, longitude float64) bool {
	if latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180 {
		return true
	}
	return false
}

func degreesToRadians(degrees float64) float64 {
	return degrees * math.Pi / 180
}

func distanceInKmBetweenEarthCoordinates(point1 model.GeoAddress, point2 model.GeoAddress) float64 {

	var earthRadiusKm, a, c float64
	earthRadiusKm = 6371

	dLat := degreesToRadians(point2.GetLatitude() - point1.GetLatitude())
	dLon := degreesToRadians(point2.GetLongitude() - point1.GetLongitude())

	lat1 := degreesToRadians(point1.GetLatitude())
	lat2 := degreesToRadians(point2.GetLatitude())

	a = math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Sin(dLon/2)*math.Sin(dLon/2)*math.Cos(lat1)*math.Cos(lat2)

	c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return earthRadiusKm * c
}

func orderListGeoadress(list []model.Station, point model.GeoAddress) []model.Station {

	sort.Slice(list[:], func(i, j int) bool {
		return distanceInKmBetweenEarthCoordinates(list[i].Position, point) < distanceInKmBetweenEarthCoordinates(list[j].Position, point)
	})

	return list
}

func FindClosestBike(list []model.Station, point model.GeoAddress) model.Station {

	for i := range orderListGeoadress(list, point) {
		if list[i].Available_bikes > 0 {
			return list[i]
			break
		}
	}
	return list[0]

}

func FindClosestStation(list []model.Station, point model.GeoAddress) model.Station {

	for i := range orderListGeoadress(list, point) {
		if list[i].Available_bike_stands > 0 {
			return list[i]
			break
		}
	}
	return list[0]

}

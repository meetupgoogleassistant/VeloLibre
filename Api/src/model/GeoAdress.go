package model

import (
	"log"
)

const ZERO float64 = 0

type GeoAddress struct {
	Lat       float64 `json:"lat,omitempty"`
	Latitude  float64 `json:"latitude,omitempty"`
	Lng       float64 `json:"lng,omitempty"`
	Longitude float64 `json:"longitude,omitempty"`
}

func (g GeoAddress) GetLatitude() float64 {
	log.Printf("Get latitudde")
	log.Printf("Lat : %f", g.Lat)
	log.Printf("Latitude : %f", g.Latitude)
	log.Printf("Lng : %f", g.Lng)
	log.Printf("Longitude : %f", g.Longitude)

	if g.Lat != ZERO {
		return g.Latitude
	}
	return g.Lat
}

func (g GeoAddress) GetLongitude() float64 {
	log.Printf("Get longitude")
	log.Printf("Lat : %f", g.Lat)
	log.Printf("Latitude : %f", g.Latitude)
	log.Printf("Lng : %f", g.Lng)
	log.Printf("Longitude : %f", g.Longitude)

	if g.Lng != ZERO {
		return g.Longitude
	}
	return g.Lng
}

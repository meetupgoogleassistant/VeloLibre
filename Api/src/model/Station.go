package model

type Station struct {
	Number   int        `json:"number,omitempty"`
	Name     string     `json:"name,omitempty"`
	Address  string     `json:"address,omitempty"`
	Position GeoAddress `json:"position,omitempty"`

	Banking               bool   `json:"banking,omitempty"`
	Bonus                 bool   `json:"bonus,omitempty"`
	Status                string `json:"status,omitempty"`
	Contract_name         string `json:"contract_name,omitempty"`
	Bike_stands           int    `json:"bike_stands,omitempty"`
	Available_bike_stands int    `json:"available_bike_stands,omitempty"`
	Available_bikes       int    `json:"available_bikes,omitempty"`
	Last_update           int    `json:"last_update,omitempty"`
}
